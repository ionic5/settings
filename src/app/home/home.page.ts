import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SettingsService } from '../settings.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private router: Router,private settingsService: SettingsService) {}

  private show_settings() {
    this.router.navigateByUrl('settings');
  }

}
