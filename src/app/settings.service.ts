import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  private usbDebugging: boolean;

  constructor() {
    this.usbDebugging = false;
  }
}
